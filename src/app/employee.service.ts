import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { IEmployee } from './employee';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http : HttpClient) { }

  // private _myUrl = "http://localhost:8080/getAllNotes"
  private _myUrl = "/assets/data/employees.json"

  getEmployees(): Observable<IEmployee[]>{
    return this.http.get<IEmployee[]>(this._myUrl).pipe(catchError(this.errorHandling))
                    
  }
  errorHandling (error : HttpErrorResponse){
    return throwError(() => error.message || "Server Error ...!")
  }
}
